;--------------------------------

  !include "MUI2.nsh"
  !include "WinVer.nsh"
  !include "x64.nsh"
  !include WordFunc.nsh
  !insertmacro VersionCompare
  !include "FileFunc.nsh"

;--------------------------------
!define OUTPUT_FILE "setup-juffed.exe"


!ifndef OUTPUT_FILE
  !Error "Not defined OUTPUT_FILE"
!endif


;--------------------------------

Function LaunchSetup
  IfSilent 0 +2
    return

FunctionEnd

;--------------------------------

!macro MoveFileToDetails file

  Push $0
  Push $1
  Push $2
  Push $3

  StrCpy $0 "${file}"

  FileOpen $1 $0 r
  IfErrors +9

    FileRead $1 $2
    IfErrors +7

    StrCpy $3 $2 2 -2
    StrCmp $3 "$\r$\n" 0 +2
      StrCpy $2 $2 -2

    StrCmp $2 "" +2
      DetailPrint $2

    Goto -7

  FileClose $1
  Delete $0

  Pop $3
  Pop $2
  Pop $1
  Pop $0

!macroend

;--------------------------------

!macro EnvToSel section env

  Push $0
  Push $1

  ReadEnvStr $0 ${env}
  StrCpy $0 $0 1
  ${Select} $0
    ${Case2} "Y" "y"
      SectionGetFlags ${section} $0
      IntOp $0 $0 | ${SF_SELECTED}
      SectionSetFlags ${section} $0
    ${Case2} "N" "n"
      SectionGetFlags ${section} $0
      IntOp $1 ${SF_SELECTED} ~
      IntOp $0 $0 & $1
      SectionSetFlags ${section} $0
  ${EndSelect}
  Pop $1
  Pop $0

!macroend

;--------------------------------

; The name of the installer
Name "JuffEd"

; The file to write
;!Warning "Using output file '${OUTPUT_FILE}'"
OutFile "${OUTPUT_FILE}"

; The default installation directory
InstallDir $PROGRAMFILES\juffed

; Registry key to check for directory (so if you install again, it will
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\juffed" "Install_Dir"

;Vista redirects $SMPROGRAMS to all users without this
RequestExecutionLevel admin

ShowInstDetails show
ShowUninstDetails show

;--------------------------------
; Pages

  !define MUI_WELCOMEPAGE_TITLE_3LINES
  !define MUI_FINISHPAGE_TITLE_3LINES

  !define MUI_FINISHPAGE_RUN
  !define MUI_FINISHPAGE_RUN_TEXT "Launch Setup"
  !define MUI_FINISHPAGE_RUN_FUNCTION LaunchSetup
  !define MUI_FINISHPAGE_RUN_NOTCHECKED

  !define MUI_FINISHPAGE_SHOWREADME ReadMe.txt
  !define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED

  !define MUI_FINISHPAGE_LINK "Giorgio Delmondo Juffed"
  !define MUI_FINISHPAGE_LINK_LOCATION https://github.com/gdelmondo/juffed

  !define MUI_FINISHPAGE_NOAUTOCLOSE

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "..\COPYING"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

  !define MUI_WELCOMEPAGE_TITLE_3LINES
  !define MUI_FINISHPAGE_TITLE_3LINES
  !define MUI_UNFINISHPAGE_NOAUTOCLOSE

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;--------------------------------
; Languages

  !insertmacro MUI_LANGUAGE "English"

;--------------------------------




	  


;--------------------------------

Section "juffed" sec_juffed

  SectionIn RO

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ; Put files there
  File "..\COPYING"
  WriteUninstaller "uninstall.exe"
  
	; Set output path to the installation directory.
  SetOutPath $INSTDIR

  File "..\build\juffed.exe"	  
  File "..\build\libjuff.dll"	
  File "..\build\libjuffed-engine-qsci.dll"	

  ; Set dependencies
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\bin\Qt5Core.dll"
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\bin\Qt5Gui.dll"
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\bin\Qt5PrintSupport.dll"
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\bin\Qt5Widgets.dll"
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\bin\Qt5WinExtras.dll"
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\bin\Qt5Network.dll"
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\bin\Qt5Xml.dll"

  File "C:\Qt\Qt5.3.1\Tools\QtCreator\bin\icuin52.dll"
  File "C:\Qt\Qt5.3.1\Tools\QtCreator\bin\icuuc52.dll"
  File "C:\Qt\Qt5.3.1\Tools\QtCreator\bin\icudt52.dll"
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\bin\Qt5Core.dll"
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\bin\qscintilla2.dll"
  File "C:\mingw\bin\mingwm10.dll"
  File "C:\mingw\bin\libcharset-1.dll"
  File "C:\Qt\Qt5.3.1\Tools\mingw482_32\bin\libgcc_s_dw2-1.dll"
  File "C:\mingw\bin\libmpfr-1.dll"
  File "C:\mingw\bin\libssp-0.dll"
  File "C:\Qt\Qt5.3.1\Tools\mingw482_32\bin\libstdc++-6.dll"
  File "C:\Qt\Qt5.3.1\Tools\mingw482_32\bin\libwinpthread-1.dll"
  
  SetOutPath "$INSTDIR\platforms"
  SetOverwrite on
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\plugins\platforms\qwindows.dll"
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\plugins\platforms\qoffscreen.dll"
  File "C:\Qt\Qt5.3.1\5.3\mingw482_32\plugins\platforms\qminimal.dll"
  
  SetOutPath "$INSTDIR\plugins"
  SetOverwrite on
  File "..\build\plugins\autosave\libautosave.dll"
  File "..\build\plugins\colorpicker\libcolorpicker.dll"
  File "..\build\plugins\doclist\libdoclist.dll"
  File "..\build\plugins\favorites\libfavorites.dll"
  File "..\build\plugins\findinfiles\libfindinfiles.dll"
  File "..\build\plugins\fm\libfm.dll"
  File "..\build\plugins\keybindings\libkeybindings.dll"
  File "..\build\plugins\sort\libsortdocument.dll"
  File "..\build\plugins\symbolbrowser\libsymbolbrowser.dll"
  File "..\build\plugins\xmlformat\libxmlformat.dll"
  
  SetOutPath "$INSTDIR\apis"
  SetOverwrite on
  File "..\apis\*.api"  

  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\juffed "Install_Dir" "$INSTDIR"

  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "DisplayName" "Giorgio Delmondo"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "Publisher" "LVD Systems srl"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "HelpLink" "https://github.com/gdelmondo/juffed"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "URLUpdateInfo" "https://github.com/gdelmondo/juffed"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "URLInfoAbout" "https://github.com/gdelmondo/juffed"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "InstallLocation" "$INSTDIR\"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "InstallSource" "$EXEDIR\"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "Language" $LANGUAGE

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "QuietUninstallString" '"$INSTDIR\uninstall.exe" /S'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\juffed" "NoRepair" 1

SectionEnd

;--------------------------------

Section "Start Menu Shortcuts" sec_shortcuts

  CreateDirectory "$SMPROGRAMS\juffed"
  CreateShortCut "$SMPROGRAMS\juffed\juffed.lnk" "$INSTDIR\juffed.exe"
  CreateShortCut "$SMPROGRAMS\juffed\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
SectionEnd

;--------------------------------

Function .onInit

    ; Check Windows version

  ${IfNot} ${AtLeastWin2000}
    MessageBox MB_YESNO|MB_DEFBUTTON2|MB_ICONEXCLAMATION \
      "The driver cannot run under below Windows 2000 System.$\n$\nContinue?" \
      /SD IDNO IDYES +2
    Abort
  ${EndIf}

  ; Set selections from enviroment
  !insertmacro EnvToSel ${sec_shortcuts}     "CNC_INSTALL_START_MENU_SHORTCUTS"

FunctionEnd

;--------------------------------

Function .onInstSuccess
  ; Call AdviseDotNETVersion
FunctionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ; Remove files and uninstaller
  Delete $INSTDIR\juffed.exe
  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\juffed\*.*"
  
  ; Remove directories used
  RMDir "$SMPROGRAMS\juffed"
  SetOutPath $TEMP
  RMDir "$INSTDIR"

SectionEnd

;--------------------------------

  ;Language strings
  LangString DESC_sec_juffed ${LANG_ENGLISH} "\
    Install juffed files. \
    "
  LangString DESC_sec_shortcuts ${LANG_ENGLISH} "\
    Add shortcuts to the Start Menu. \
    "

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${sec_juffed} $(DESC_sec_juffed)
    !insertmacro MUI_DESCRIPTION_TEXT ${sec_shortcuts} $(DESC_sec_shortcuts)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
