#include <QString>
#include <QStringList>
#include <QDomDocument>
#include <QDomElement>
#include <QFileInfo>

#include "SessionManager.h"
#include "AppInfo.h"
#include "Log.h"


bool SessionManager::deleteSession ( QString path )
{
    QString fileName = AppInfo::sessionListFile();
    QStringList sessionsList;
    
    loadSessionList ( sessionsList );
    
    // remove session
	int idx = sessionsList.indexOf(path);	
	if ( idx >= 0 ) {
		sessionsList.removeAt(idx);
	}
    
    // save session list
    QFile filew(fileName);
	if ( !filew.open(QIODevice::WriteOnly) ) {
		Log::warning(QString("Can't open file '%1' for writing").arg(fileName));
        return false;
	}
	else {
		Log::debug(QString("SessionList '%1' opened successfully for writing").arg(fileName), true);
	}    
    
    QDomDocument docw("JuffEd_SessionsList");
    QDomElement root = docw.createElement("SessionList");
    docw.appendChild(root);
    
    foreach ( QString path, sessionsList ) {
        QDomElement tag = docw.createElement("session");
		tag.setAttribute("path", path);
        root.appendChild(tag);
    }
      
    filew.write ( docw.toByteArray() );
	filew.close();
}

bool SessionManager::loadSessionList(QStringList &sessionsList)     
{    
    QString fileName = AppInfo::sessionListFile();
    QDomDocument doc("JuffEd_SessionsList");
    
    QFile file(fileName);
	if ( !file.open(QIODevice::ReadOnly) ) {
		Log::warning(QString("Can't open file '%1'").arg(fileName));
		return false;
	}
	else {
		Log::debug(QString("SessionList '%1' opened successfully").arg(fileName), true);
	}
    
    QString err;
	int errLine, errCol;
	if ( !doc.setContent(&file, &err, &errLine, &errCol) ) {
		Log::debug(QString("File %1: XML reading error: '%2', line %3, column %4")
				.arg(fileName).arg(err).arg(errLine).arg(errCol));
		file.close();
		return false;
	}
	else {
		Log::debug(QString("SessionList '%1' was parsed successfully").arg(fileName), true);
	}
	file.close();

	QDomElement docElem = doc.documentElement();
    sessionsList.clear();
    
    QDomNode subNode = docElem.firstChild();

	while ( !subNode.isNull() ) {
		QDomElement subEl = subNode.toElement();
		QString tagName = subEl.tagName().toLower();
		if ( tagName.compare("session") == 0 ) {
			QString path = subEl.attribute("path", "");

			if ( !path.isEmpty() ) {
                sessionsList.append(path);
			}
		}
		subNode = subNode.nextSibling();
	}
    
    return true;
}

bool SessionManager::addToRecentSessions( QString path, bool reload ) {
    QString fileName = AppInfo::sessionListFile();
    QStringList sessionsList;
    
    loadSessionList ( sessionsList );
    
    // append current session
    sessionsList.prepend(path);
    sessionsList.removeDuplicates();
    
    // save session list
    QFile filew(fileName);
	if ( !filew.open(QIODevice::WriteOnly) ) {
		Log::warning(QString("Can't open file '%1' for writing").arg(fileName));
        return false;
	}
	else {
		Log::debug(QString("SessionList '%1' opened successfully for writing").arg(fileName), true);
	}    
    
    QDomDocument docw("JuffEd_SessionsList");
    QDomElement root = docw.createElement("SessionList");
    docw.appendChild(root);
    
    foreach ( QString path, sessionsList ) {
        QDomElement tag = docw.createElement("session");
		tag.setAttribute("path", path);
        root.appendChild(tag);
    }
      
    filew.write ( docw.toByteArray() );
	filew.close();
}