#include <QDebug>

/*
JuffEd - An advanced text editor
Copyright 2007-2010 Mikhail Murzin

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License 
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "JuffScintilla.h"

#include "QSciSettings.h"

#include <QApplication>
#include <QClipboard>
#include <QScrollBar>
#include <QMimeData>

#include <Qsci/qscicommandset.h>

#include "Constants.h"
#include "Log.h"
#include "Utils.h"

#define WORD_HIGHLIGHT     1
#define SEARCH_HIGHLIGHT   2

namespace Juff {

JuffScintilla::JuffScintilla() : QsciScintilla() {
	initHighlightingStyle(WORD_HIGHLIGHT, QSciSettings::get(QSciSettings::WordHLColor));
	initHighlightingStyle(SEARCH_HIGHLIGHT, QSciSettings::get(QSciSettings::SearchHLColor));
	
	contextMenu_ = new QMenu();
	CommandStorageInt* st = Juff::Utils::commandStorage();
	contextMenu_->addAction(st->action(EDIT_UNDO));
	contextMenu_->addAction(st->action(EDIT_REDO));
	contextMenu_->addSeparator();
	contextMenu_->addAction(st->action(EDIT_CUT));
	contextMenu_->addAction(st->action(EDIT_COPY));
	contextMenu_->addAction(st->action(EDIT_PASTE));
	contextMenu_->addSeparator();
/*	contextMenu_->addAction(st->action(SEARCH_FIND));
	contextMenu_->addAction(st->action(SEARCH_FIND_NEXT));
	contextMenu_->addAction(st->action(SEARCH_FIND_PREV));
	contextMenu_->addAction(st->action(SEARCH_REPLACE));
	contextMenu_->addSeparator();*/
	contextMenu_->addAction(st->action(SEARCH_GOTO_LINE));
	
	connect(this, SIGNAL(linesChanged()), this, SLOT(updateLineNumbers()));
	
	// list of commands we want to disable initially
	QList<int> cmdsToRemove;
	cmdsToRemove << (Qt::Key_D | Qt::CTRL) << (Qt::Key_L | Qt::CTRL) 
	             << (Qt::Key_T | Qt::CTRL) << (Qt::Key_U | Qt::CTRL) 
	             << (Qt::Key_U | Qt::CTRL | Qt::SHIFT);
	
	QsciCommandSet* set = standardCommands();
	QList< QsciCommand*> cmds = set->commands();
	foreach (QsciCommand* cmd, cmds) {
		if ( cmdsToRemove.contains(cmd->key()) )
			cmd->setKey(0);
		if ( cmdsToRemove.contains(cmd->alternateKey()) )
			cmd->setAlternateKey(0);
	}
}

JuffScintilla::~JuffScintilla() {
	delete contextMenu_;
}

QString JuffScintilla::wordUnderCursor() {
	int line, col;
	getCursorPosition(&line, &col);
	QString str = text(line);
	int startPos = str.left(col).lastIndexOf(QRegExp("\\b"));
	int endPos = str.indexOf(QRegExp("\\b"), col);
	if ( startPos >= 0 && endPos >= 0 && endPos > startPos )
		return str.mid(startPos, endPos - startPos);
	else
		return "";
}

/*bool JuffScintilla::findML(const QString& s, const DocFindFlags& flags) {
	JUFFENTRY;
	QString text = this->text();
	QRegExp rx(s);
	
	long pos = -1;
	if ( flags.backwards ) {
		long cPos;
		if ( hasSelectedText() ) {
			int line1, col1, line2, col2;
			getSelection(&line1, &col1, &line2, &col2);
			cPos = lineColToPos(line1, col1);
		}
		else {
			cPos = curPos();
		}

		pos = text.left(cPos).lastIndexOf(rx);
	}
	else {
		long cPos;
		if ( hasSelectedText() ) {
			int line1, col1, line2, col2;
			getSelection(&line1, &col1, &line2, &col2);
			cPos = lineColToPos(line2, col2);
		}
		else {
			cPos = curPos();
		}

		pos = text.indexOf(rx, cPos);
	}

	if ( pos >= 0 ) {
		int line1, col1, line2, col2;
		posToLineCol(pos, line1, col1);
		posToLineCol(pos + rx.matchedLength(), line2, col2);
		setSelection(line1, col1, line2, col2);
		ensureCursorVisible();
		return true;
	}
	else {
		return false;
	}
}
*/

void JuffScintilla::dragEnterEvent(QDragEnterEvent* e) {
	if ( !e->mimeData()->hasUrls() )
		QsciScintilla::dragEnterEvent(e);
}

void JuffScintilla::dropEvent(QDropEvent* e) {
	if ( !e->mimeData()->hasUrls() )
		QsciScintilla::dropEvent(e);
}

void JuffScintilla::contextMenuEvent(QContextMenuEvent* e) {
	QPoint point = e->pos();
	
	int mWidth = marginWidth(0) + marginWidth(1); // width of two margins: markers' and line numbers'
	if ( point.x() <= mWidth ) {
		mWidth += marginWidth(2) + 5; // just in case :)
		long pos = SendScintilla(SCI_POSITIONFROMPOINTCLOSE, point.x() + mWidth, point.y());
		int line = SendScintilla(SCI_LINEFROMPOSITION, pos);
		
		setCursorPosition(line, 0);
		emit markersMenuRequested(mapToGlobal(point));
		return;
	}
	
	int line, col;
	long position = SendScintilla(SCI_POSITIONFROMPOINTCLOSE, point.x(), point.y());
	lineIndexFromPosition(position, &line, &col);
	
	emit contextMenuCalled(line, col);
	contextMenu_->exec(e->globalPos());
}

void JuffScintilla::focusInEvent(QFocusEvent* e) {
	parentWidget()->setFocusProxy(this);
	QsciScintilla::focusInEvent(e);
	emit focusReceived();
}

void JuffScintilla::focusOutEvent(QFocusEvent* e) {
	cancelList();
	QsciScintilla::focusOutEvent(e);
}

void JuffScintilla::wheelEvent(QWheelEvent* e) {
	if ( e->modifiers() & Qt::ControlModifier ) {
		if ( e->delta() < 0 ) {
			zoomOut();
		}
		else if ( e->delta() > 0 ) {
			zoomIn();
		}
	}
	else {
		QsciScintilla::wheelEvent(e);
	}
}

void JuffScintilla::cancelRectInput() {
	SendScintilla(SCI_CANCEL);
}

void JuffScintilla::setSelection(int lineFrom, int indexFrom, int lineTo, int indexTo)
{
	Log::debug(QString("setSelection %1 %2 %3 %4").arg(lineFrom).arg(indexFrom).arg(lineTo).arg(indexTo), true);
    SendScintilla(SCI_SETSELECTION, positionFromLineIndex(lineFrom, indexFrom), positionFromLineIndex(lineTo, indexTo));
}

void JuffScintilla::juff_insertAt(const QString &text, int line, int index)
{
    int lnsz = SendScintilla(SCI_GETLINEENDPOSITION, line);
    while ( lnsz < index ) {
        juff_insertAtPos(QString(" "), positionFromLineIndex(line, lnsz));
        lnsz++;
    }
    juff_insertAtPos(text, positionFromLineIndex(line, index));
}

// Insert the given text at the given position.
void JuffScintilla::juff_insertAtPos(const QString &text, int pos)
{
    SendScintilla(SCI_BEGINUNDOACTION);
    SendScintilla(SCI_INSERTTEXT, pos,
            ScintillaBytesConstData(textAsBytes(text)));
    SendScintilla(SCI_ENDUNDOACTION);
}

void JuffScintilla::keyPressEvent(QKeyEvent* e) {
	
	if ( SendScintilla(SCI_SELECTIONISRECTANGLE) ) {                                                                		
		int line, col;
		getCursorPosition(&line, &col);

		int line1, col1, line2, col2;
		QList<JuffSciRecSel> selection = getRectangularSelection();
		
		//Log::debug(QString("SCI_SELECTIONISRECTANGLE"), true);

		switch ( e->key() ) {
			case Qt::Key_Escape :
				//setSelection(line, col, line, col);
				cancelRectInput();
				break;

			case Qt::Key_Left:
			case Qt::Key_Right:
			case Qt::Key_Up:
			case Qt::Key_Down:
				if ( !(e->modifiers() & Qt::AltModifier) ) {
					//setSelection(line, col, line, col);
					cancelRectInput();
				}
				else {
					QsciScintilla::keyPressEvent(e);
				}
				break;

			case Qt::Key_Backspace:
				
				beginUndoAction();
				foreach ( JuffSciRecSel sel, selection ) {
					if ( sel.caret != sel.anchor ) {
						deleteRectSelection(sel.line, sel.caret, sel.line, sel.anchor);
					} else {
						if ( sel.v_caret == 0 ) {
							deleteRectSelection(sel.line, sel.caret-1, sel.line, sel.anchor);
						}
					}
				}
				endUndoAction();
					
				Log::debug(QString("Qt::Key_Backspace ended %1").arg(SendScintilla(SCI_SELECTIONISRECTANGLE)), true);
				break;
				
			case Qt::Key_Delete :
				beginUndoAction();
				foreach ( JuffSciRecSel sel, selection ) {
					if ( sel.caret != sel.anchor ) {
						deleteRectSelection(sel.line, sel.caret, sel.line, sel.anchor);
					} else {
						if ( sel.v_caret == 0 ) {
							deleteRectSelection(sel.line, sel.caret, sel.line, sel.anchor+1);
						}
					}
				}
				endUndoAction();
					
				Log::debug(QString("Qt::Key_Delete ended %1").arg(SendScintilla(SCI_SELECTIONISRECTANGLE)), true);
				break;

			default:
				QsciScintilla::keyPressEvent(e);
				/*if ( e->modifiers() & Qt::ControlModifier ) {
					break;
				}
				
				QString t = e->text();
				if ( t.length() < 0 ) {
					break;
				}
				
				beginUndoAction();
				foreach ( JuffSciRecSel sel, selection ) {
					if ( sel.caret != sel.anchor ) {
						if ( t.length() ) {
							deleteRectSelection(sel.line, sel.caret, sel.line, sel.anchor);
						}
					}
					
					while ( sel.v_caret ) {
						insertAt(QString(" "), sel.line, sel.caret);
						sel.caret++;
						sel.v_caret--;
					}
					
					insertAt(t, sel.line, sel.caret);
				}
				if ( e->key() != Qt::Key_Enter && e->key() != Qt::Key_Return ) {
					setSelection(line1, col1 + t.length(), line2, col1 + t.length());
					SendScintilla(SCI_SETSELECTIONMODE, SC_SEL_RECTANGLE);
				}
				endUndoAction(); */
		}
	}
	else {
		// not rectangular selection
		bool eclipseStyle = QSciSettings::get(QSciSettings::JumpOverWordParts);

		Log::debug(QString("NOT SCI_SELECTIONISRECTANGLE"), true);
		
		int key = e->key();
		if ( e->modifiers() & Qt::ControlModifier ) {
			// control hotkeys
			switch ( key ) {
				case Qt::Key_Left :
					if ( eclipseStyle ) {
						if ( e->modifiers() & Qt::ShiftModifier )
							SendScintilla(SCI_WORDPARTLEFTEXTEND);
						else
							SendScintilla(SCI_WORDPARTLEFT);
					}
					else {
						QsciScintilla::keyPressEvent(e);
					}
					break;
				
				case Qt::Key_Right :
					if ( eclipseStyle ) {
						if ( e->modifiers() & Qt::ShiftModifier )
							SendScintilla(SCI_WORDPARTRIGHTEXTEND);
						else
							SendScintilla(SCI_WORDPARTRIGHT);
					}
					else {
						QsciScintilla::keyPressEvent(e);
					}
					break;
				
				case Qt::Key_Backspace :
					if ( eclipseStyle ) {
						beginUndoAction();
						SendScintilla(SCI_WORDPARTLEFTEXTEND);
						removeSelectedText();
						endUndoAction();
					}
					else {
						QsciScintilla::keyPressEvent(e);
					}
					break;
				
				case Qt::Key_Delete :
					if ( eclipseStyle ) {
						beginUndoAction();
						SendScintilla(SCI_WORDPARTRIGHTEXTEND);
						removeSelectedText();
						endUndoAction();
					}
					else {
						QsciScintilla::keyPressEvent(e);
					}
					break;
				
					case Qt::Key_Space :
					{
						// Dirty hack but looks like it works :)
						// To display auto-completion box we "imitate"
						// entering the previous char. We do not enter it,
						// we just emit the notifying signal that triggers
						// displaying the auto-completion box.
						int pos = SendScintilla(SCI_GETCURRENTPOS);
						if ( pos > 0 ) {
							int ch = SendScintilla(SCI_GETCHARAT, pos - 1);
							emit SCN_CHARADDED(ch);
						}
					}
						break;
					
				default:
					if ( e->modifiers() & Qt::AltModifier ) {
						return;
					}
					else {
						QsciScintilla::keyPressEvent(e);
					}
			}
		}
		else {
			// hotkeys without Control
			switch ( key ) {
				case Qt::Key_Enter :
				case Qt::Key_Return :
					beginUndoAction();
					QsciScintilla::keyPressEvent(e);
					endUndoAction();
					break;
				
				case Qt::Key_Escape :
					clearHighlighting();
					emit escapePressed();
					QsciScintilla::keyPressEvent(e);
					break;
				
				default:
					QsciScintilla::keyPressEvent(e);
			}
		}
	}
}

void JuffScintilla::cut() {
	if ( SendScintilla(SCI_SELECTIONISRECTANGLE) ) {
		beginUndoAction();
		copy();
		deleteRectSelection();
		endUndoAction();
	}
	else {
		QsciScintilla::cut();
	}
}

void JuffScintilla::paste() {
	QString originalText = QApplication::clipboard()->text();
	QString convertedText;
	if ( originalText.contains(LineSeparatorRx) ) {
		QStringList lines = originalText.split(LineSeparatorRx);
		switch ( eolMode() ) {
			case EolWindows : convertedText = lines.join("\r\n"); break;
			case EolUnix    : convertedText = lines.join("\n"); break;
			case EolMac     : convertedText = lines.join("\r"); break;
		}
		QApplication::clipboard()->setText(convertedText);
	}
	
	if ( SendScintilla(SCI_SELECTIONISRECTANGLE) ) {
		QString text = QApplication::clipboard()->text();
		int line1, col1, line2, col2;
		getOrderedSelection(line1, col1, line2, col2);
		
		beginUndoAction();
		deleteRectSelection(line1, col1, line2, col2);
		for ( int line = line2; line >= line1; --line ) {
			insertAt(text, line, col1);
		}
		endUndoAction();
	}
	else {
		QsciScintilla::paste();
	}
	
	// restore the original clipboard content
	QApplication::clipboard()->setText(originalText);
}

QList<JuffSciRecSel> JuffScintilla::getRectangularSelection() {
	int line1, col1, line2, col2, col3;
	QList<JuffSciRecSel> list;
    int nbSel = SendScintilla(SCI_GETSELECTIONS);
	
	lineIndexFromPosition(SendScintilla(SCI_GETSELECTIONSTART), &line1, &col1);
	lineIndexFromPosition(SendScintilla(SCI_GETSELECTIONEND), &line2, &col2);
	
	line1 = qMin(line1, line2);
	line2 = qMax(line1, line2);
	
	for ( int i=0; i < nbSel; i++ ) {
		int absPosSelStartPerLine = SendScintilla(SCI_GETSELECTIONNANCHOR, i);
		int absPosSelEndPerLine = SendScintilla(SCI_GETSELECTIONNCARET, i);
		int nbVirtualAnchorSpc = SendScintilla(SCI_GETSELECTIONNANCHORVIRTUALSPACE, i);
		int nbVirtualCaretSpc = SendScintilla(SCI_GETSELECTIONNCARETVIRTUALSPACE, i);
		
		JuffSciRecSel sel; 		
		
		lineIndexFromPosition ( absPosSelStartPerLine, &line2, &col2 );
		sel.line  = line2;
		lineIndexFromPosition ( absPosSelEndPerLine, &line2, &col3 );
		sel.caret  = qMin(col2, col3); 		
		sel.anchor = qMax(col2, col3);
		sel.v_caret  = nbVirtualCaretSpc; 
		sel.v_anchor = nbVirtualAnchorSpc;
		
		
		Log::debug(QString("MSEL : %1 %2 %3 %4 %5").arg(sel.line).arg(sel.caret).arg(sel.anchor).arg(sel.v_caret).arg(sel.v_anchor), true);
				
		list.push_back(sel);
	}
	    
	return list;	
}

void JuffScintilla::getOrderedSelection(int& rLine1, int& rCol1, int& rLine2, int& rCol2) {
	int line1, col1, line2, col2;
	//getSelection(&line1, &col1, &line2, &col2);
	lineIndexFromPosition(SendScintilla(SCI_GETSELECTIONSTART), &line1, &col1);
	lineIndexFromPosition(SendScintilla(SCI_GETSELECTIONEND), &line2, &col2);
	
	rLine1 = qMin(line1, line2);
	rCol1 = qMin(col1, col2);
	rLine2 = qMax(line1, line2);
	rCol2 = qMax(col1, col2);
}

void JuffScintilla::deleteRectSelection() {
	int line1, col1, line2, col2;
	getOrderedSelection(line1, col1, line2, col2);
	deleteRectSelection(line1, col1, line2, col2);
}

void JuffScintilla::deleteRectSelection(int line1, int col1, int line2, int col2) {
	for ( int line = line1; line <= line2; ++line ) {
		QString lineStr = text(line).section(LineSeparatorRx, 0, 0);
		int length = lineStr.length();
		if ( col1 >= length ) {
			continue;
		}
		setSelection(line, col1, line, qMin(col2, length));
		removeSelectedText();
	}
}

void JuffScintilla::showLineNumbers(bool show) {
	showLineNumbers_ = show;
	updateLineNumbers();
}

bool JuffScintilla::lineNumbersVisible() const {
	return showLineNumbers_;
}

void JuffScintilla::updateLineNumbers() {
	if ( showLineNumbers_ ) {
		QString str = QString("00%1").arg(lines());
		setMarginWidth(1, str);
	}
	else {
		setMarginWidth(1, 0);
	}
}

void JuffScintilla::clearHighlighting() {
	SendScintilla(SCI_SETINDICATORCURRENT, WORD_HIGHLIGHT);
	SendScintilla(SCI_INDICATORCLEARRANGE, 0, length());
	SendScintilla(SCI_SETINDICATORCURRENT, SEARCH_HIGHLIGHT);
	SendScintilla(SCI_INDICATORCLEARRANGE, 0, length());
}

/*
 * It changes document's current selection (if any), so you'd better keep it before 
 * calling this function and restore it later.
 */
void JuffScintilla::highlight(HLMode, int row1, int col1, int row2, int col2) {
	int pos1 = positionFromLineIndex(row1, col1);
	int pos2 = positionFromLineIndex(row2, col2);
	highlight(pos1, pos2, SEARCH_HIGHLIGHT);
}

// TODO : refactor this method
void JuffScintilla::highlightText(HLMode mode, const Juff::SearchParams& params) {
	clearHighlighting();
	QString text = params.findWhat;
	
	if ( text.length() < 1 )
		return;
	
	int initialLine, initialCol;
	getCursorPosition(&initialLine, &initialCol);
	int scrollPos = verticalScrollBar()->value();
	
	if ( mode == HLCurrentWord ) {
		int line = 0, col = 0;
		while ( findFirst(text, false, false, true, false, true, line, col) ) {
			int start = SendScintilla(SCI_GETSELECTIONSTART);
			int end = SendScintilla(SCI_GETSELECTIONEND);
			highlight(start, end, WORD_HIGHLIGHT);
			lineIndexFromPosition(end, &line, &col);
		}
	}
	
	setCursorPosition(initialLine, initialCol);
	verticalScrollBar()->setValue(scrollPos);
}

void JuffScintilla::highlight(int start, int end, int ind) {
	SendScintilla(SCI_SETINDICATORCURRENT, ind);
	SendScintilla(SCI_INDICATORFILLRANGE, start, end - start);
}

void JuffScintilla::initHighlightingStyle(int id, const QColor &color) {
	SendScintilla(SCI_INDICSETSTYLE, id, INDIC_ROUNDBOX);
	SendScintilla(SCI_INDICSETUNDER, id, true);
	SendScintilla(SCI_INDICSETFORE, id, color);
	SendScintilla(SCI_INDICSETALPHA, id, 50);
}

}

