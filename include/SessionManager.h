#ifndef __JUFF_SESSION_MANAGER__
#define __JUFF_SESSION_MANAGER__

#include "LibConfig.h"

namespace Juff {
	class SessionManager;
}

class QStringList;

class LIBJUFF_EXPORT SessionManager {
public:
    static bool loadSessionList(QStringList &sessionsList);
    static bool addToRecentSessions( QString path, bool reload=true );
	static bool deleteSession ( QString path );
};

#endif

